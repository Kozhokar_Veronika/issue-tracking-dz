#ifndef Item_H
#define Item_H
#include "AbstractItemMenu.h"

namespace IEG
{
	class Item : public ItemMenu
	{
	public:
		Item(const char*, Func);

		const char* get();

		void printItem();

		int run();

	};
}

#endif