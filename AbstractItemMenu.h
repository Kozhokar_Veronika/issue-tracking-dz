#ifndef AbstractItemMenu
#define AbstractItemMenu
#include "Administrator.h"
namespace IEG
{

	typedef int(*Func)();
	class ItemMenu
	{
	protected:
		const char* m_item_name = nullptr;//��������� �� C-������ �������� ������
		
		Func m_func = nullptr;//��������� �� ����������� �������
	public:
		ItemMenu(const char*, Func);

		virtual const char* get() = 0;
		
		virtual void printItem() = 0;
		
		virtual int run() = 0;
		

	};
}
#endif 