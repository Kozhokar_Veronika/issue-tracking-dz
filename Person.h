#ifndef Person_H
#define Person_H
#include <iostream>
class Person
{
public:
	std::string m_name{};
	std::string m_surname{};
	int m_age{};

	Person(std::string, std::string, int);
	Person();
	void setName(std::string);
	void setSurname(std::string);
	void setAge(int);
	std::string getName();
	std::string getSurname();
	int getAge();
};
#endif

