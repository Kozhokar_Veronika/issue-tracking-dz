#ifndef CMenu
#define CMenu
#include "AbstractItemMenu.h"
#include <iostream>
#include <fstream>

namespace IEG
{
	class Cmenu
	{
		int m_select = -1;//�������� ���� ������������
		bool m_running = false;//�������� �� ���������� ����
		std::string m_title = nullptr;//������ �� ������ � ���������� ����
		unsigned int m_count;//���-�� ������� ����
		ItemMenu* m_items = nullptr;//��������� �� ������ ��������
	public:
		Cmenu(std::string, ItemMenu*, unsigned int);
		int getSelect();
		void setSelect(int select);
		bool getRunning();
		std::string getTitle();
		unsigned int getCount();
		ItemMenu* getItems();
		void printItems () const;
		friend std::ostream& operator<< (std::ostream& out, const Cmenu& menu);
		friend std::istream& operator>> (std::istream& out, Cmenu& menu);
		int runCommand();
		int readEnter();
	};
}

#endif 

