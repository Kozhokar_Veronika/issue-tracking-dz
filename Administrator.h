#ifndef Administrator_H
#define Administrator_H

#include "User.h"
class Administrator :
    public User
{
public:
    Administrator(std::string, std::string, int, std::string, std::string, bool);
    Administrator();
};

#endif 
