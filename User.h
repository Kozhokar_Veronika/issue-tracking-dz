#ifndef User_H
#define User_H
#include "Person.h"
#include <ctime>
#include <fstream>
#include <vector>
#include "Product.h"

class User : public Person
{
	time_t m_id{};
	tm m_data{};
	std::string m_password{};
	std::string m_login{};
	std::vector<time_t> m_basket;
public:
	bool m_level{};
	User();
	User(std::string name, std::string surname, int age, std::string login, std::string password, bool);
	
	void setLogin(std::string);
	void setPassword(std::string);
	void setid(time_t);
	void setData(tm);
	void setLevel(bool);
	void setBasket(std::vector<time_t>*);
	void setBaskid(time_t id);

	std::string getLogin();
	std::string getPassword();
	std::tm getData();
	time_t getid();
	std::vector<time_t> getBasket();


	void ChangePass(std::string);

	bool operator== (const User& it) const
	{
		return m_id == it.m_id;
	}
	bool operator!= (const User& it) const
	{
		return m_id != it.m_id;
	}
	//friend std::ostream& operator<<(std::ostream&, const User&);
	//friend istream& operator>>(istream&, User&);
};

#endif