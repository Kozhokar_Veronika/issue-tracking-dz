#ifndef func_H
#define func_H
#include <iostream>
#include "Person.h"
#include "User.h"
#include "Administrator.h"
#include "Product.h"

void ReadUser();
void WriteUser();
void PrintUser(User& use);
void PrintAdmin(Administrator& use);
User* MakeUser();
Administrator* MakeAdmin();
int AddUser();
int AddAdmin();
int Exi();
int InfUser();
int InfAdmin();
int Add();
int Entry();
int EntUser();
int EntAdmin();

int AddProd();
Product* MakeProduct();


int DeleteProduct();


int ChangeProduct();
int ChangeName();
int ChangeType();
int ChangePrice();
int ChangeCount();

int Basket();

int Find();

void ReadProduct();
void WriteProduct();
int PrintProd();
void PrintProduct(Product& prod);
#endif