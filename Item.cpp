#include "Item.h"
#include <iostream>
using namespace IEG;

const char* Item::get()
{
	return m_item_name;
}

void Item::printItem()
{
	std::cout << m_item_name << std::endl;
}

int Item::run()
{
	return m_func();
}

Item::Item(const char* item_menu = nullptr, Func function = nullptr) : ItemMenu(item_menu, function) {}