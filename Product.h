#ifndef Product_H
#define Product_H
#include <string>

class Product
{
	std::string m_name;
	std::string m_type;
	unsigned int m_price;
	unsigned int m_count;
	time_t m_id{};
public:
	Product();
	Product(std::string, std::string, unsigned int, unsigned int);
	void setName(std::string);
	void setType(std::string);
	void setPrice(unsigned int);
	void setCount(unsigned int);
	void setid(time_t);
	std::string getName();
	std::string getType();
	unsigned int getPrice();
	unsigned int getCount();
	time_t getid();
};

#endif // !Product_H
