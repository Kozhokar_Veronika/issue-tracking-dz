#include "Administrator.h"

Administrator::Administrator(std::string name, std::string surname, int age, std::string login, std::string password, bool level = 1) :
	User(name, surname, age, login, password, level) {};

Administrator::Administrator() : User() { setLevel(1); };

