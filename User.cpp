#include "User.h"
#include <ctime>


User::User(std::string name, std::string surname, int age, std::string login, std::string password, bool level = 0) : Person(name, surname, age), m_password(password), m_login(login), m_level(0), m_basket()
{
	time_t now = time(0);
	localtime_s(&m_data, &now);
	m_data.tm_year += 1900;
	m_data.tm_mon += 1;
	m_id = time(0);
}

User::User():Person(), m_password(" "), m_login(" "), m_level(0), m_basket()
{
	time_t now = time(0);
	localtime_s(&m_data, &now);
	m_data.tm_year += 1900;
	m_data.tm_mon += 1;
	m_id = time(0);
}



void User::setLogin(std::string login)
{
	m_login = login;
};

void User::setPassword(std::string password)
{
	m_password = password;
}


void User::setid(time_t id)
{
	m_id = id;
}
void User::setData(tm data)
{
	m_data = data;
}
void User::setLevel(bool level)
{
	m_level = level;
}
void User::setBasket(std::vector<time_t>* bas)
{
	m_basket = *bas;
}
void User::setBaskid(time_t id)
{
	m_basket.push_back(id);
}
void User::ChangePass(std::string password)
{
	bool val{};
	std::string temp{};
	while (val == 0)
	{
		std::cout << "\n������� ������ ������:   ";
		std::cin >> temp;
		if (temp == m_password)
		{
			val = 1;
			std::cout << "\n������� ����� ������:   ";
			std::cin >> temp;
			m_password = temp;
		}
		else {
			std::cout << "\n������ ��������.";
		}
	}
}

std::string User::getLogin()
{
	return m_login;
}
std::string User::getPassword()
{
	return m_password;
}
std::tm User::getData()
{
	return m_data;
}
time_t User::getid()
{
	return m_id;
}
std::vector<time_t> User::getBasket()
{
	return m_basket;
}

/*std::ostream& operator<<(std::ostream& out, const tm ltm)
{
	out << 1900 + ltm.tm_year << ' ' << ltm.tm_mday << ' ' << 1 + ltm.tm_mon;
	out << ltm.tm_hour << ':' << ltm.tm_min << ':' << ltm.tm_sec;
	return out;
}
std::ostream& operator<<(std::ostream& out, const User& obj) {
	out << obj.m_name;
	out << obj.m_surname;
	out << obj.m_age;
	out << obj.m_login;
	out << obj.m_password;
	out << obj.m_id;
	out << obj.m_data.tm_year << obj.m_data.tm_mon << obj.m_data.tm_mday << obj.m_data.tm_hour << obj.m_data.tm_min << obj.m_data.tm_sec;
	out << obj.m_level;
	out << "Name: " << obj.m_name << "\n Surname: " << obj.m_surname << "\n Age: " << obj.m_age;
	return out;
};*/