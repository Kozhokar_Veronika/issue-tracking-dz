#include <iostream>
#include <vector>
#include "Person.h"
#include "User.h"
#include "Administrator.h"
#include <fstream>
#include "CMenu.h"
#include "Item.h"
#include <string>
#include "Product.h"
#include "func.h"
std::vector<Administrator> userad;
std::vector<User> user;
User s_user = User();
Administrator s_ad = Administrator();
std::vector<Product> product;
std::vector<Product> temp;
Product filt = Product();

void ReadUser()
{
	std::ifstream file;
	file.open("Users.txt");
	User* us = new User;
	int n{};
	std::string str;
	int count{};
	time_t id;
	tm data;
	if (file.is_open())
	{
		file >> n;
		for (int i = 0; i < n; i++)
		{
			file >> str;
			us->setName(str);
			file >> str;
			us->setSurname(str);
			file >> str;
			us->setLogin(str);
			file >> str;
			us->setPassword(str);
			file >> count;
			us->setAge(count);
			file >> id;
			us->setid(id);
			file >> us->m_level;
			file >> data.tm_year;
			file >> data.tm_mon;
			file >> data.tm_mday;
			file >> data.tm_hour;
			file >> data.tm_min;
			file >> data.tm_sec;
			us->setData(data);
			file >> count;
			for (int j = 0; j < count; j++)
			{
				file >> id;
				us->setBaskid(id);
			}
			user.push_back(*us);
		}

	}
	file.close();
}
void WriteUser()
{
	User* us = new User;
	std::ofstream f;
	f.open("Users.txt");
	if (f.is_open())
	{
		f << user.size() << std::endl;
		for (int i = 0; i < user.size(); i++)
		{
			f << user[i].m_name << std::endl;
			f << user[i].m_surname << std::endl;
			f << user[i].getLogin() << std::endl;
			f << user[i].getPassword() << std::endl;
			f << user[i].getAge() << std::endl;
			f << user[i].getid() << std::endl;
			f << user[i].m_level << std::endl;
			f << user[i].getData().tm_year << std::endl;
			f << user[i].getData().tm_mon << std::endl;
			f << user[i].getData().tm_mday << std::endl;
			f << user[i].getData().tm_hour << std::endl;
			f << user[i].getData().tm_min << std::endl;
			f << user[i].getData().tm_sec << std::endl;
			f << user[i].getBasket().size() << std::endl;
			for (int j = 0; j < user[i].getBasket().size(); j++)
			{
				f << user[i].getBasket()[j] << std::endl;
			}
		}
	}
	f.close();
}

void PrintUser(User& use)
{
	std::cout << "\n���:   " << use.getName();
	std::cout << "\n������� (������ ���):   " << use.getSurname();
	std::cout << "\n�����:   " << use.getLogin();
	std::cout << "\nID:   " << use.getid();
	std::cout << "\n�������:   " << use.getAge();
	std::cout << std::endl << use.getData().tm_year << '.' << use.getData().tm_mon << '.' << use.getData().tm_mday << ' ';
	std::cout << use.getData().tm_hour << ':' << use.getData().tm_min << ':' << use.getData().tm_sec << std::endl;
}

void PrintAdmin(Administrator& use)
{
	std::cout << "\n\n���:   " << use.getName();
	std::cout << "\n������� (������ ���):   " << use.getSurname();
	std::cout << "\n�����:   " << use.getLogin();
	std::cout << "\n�������:   " << use.getAge();
	std::cout << std::endl << 1900 + use.getData().tm_year << '.' << 1 + use.getData().tm_mon << '.' << use.getData().tm_mday << ' ';
	std::cout << use.getData().tm_hour << ':' << use.getData().tm_min << ':' << use.getData().tm_sec;
}
User* MakeUser()
{
	User* use = new User;
	std::string temp{};
	std::cout << "\n������� ���:   ";
	std::cin >> temp;
	use->setName(temp);
	std::cout << "\n������� ������� (������ ���):   ";
	std::cin >> use->m_surname;
	std::cout << "\n������� �������:   ";
	std::cin >> use->m_age;
	std::cout << "\n������� �����:   ";
	std::cin >> temp;
	use->setLogin(temp);
	std::cout << "\n������� ������:   ";
	std::cin >> temp;
	use->setPassword(temp);
	return use;
	delete use;
}
Administrator* MakeAdmin()
{
	Administrator* adm = new Administrator;
	std::string temp{};
	std::cout << "\n������� ���:   ";
	std::cin >> temp;
	adm->setName(temp);
	std::cout << "\n������� ������� (������ ���):   ";
	std::cin >> adm->m_surname;
	std::cout << "\n������� �������:   ";
	std::cin >> adm->m_age;
	std::cout << "\n������� �����:   ";
	std::cin >> temp;
	adm->setLogin(temp);
	std::cout << "\n������� ������:   ";
	std::cin >> temp;
	adm->setPassword(temp);
	return adm;
	delete adm;
}
int AddUser()
{
	std::system("cls");
	ReadUser();
	User* us = new User;
	us = MakeUser();
	user.push_back(*us);
	WriteUser();
	user.clear();
	std::cout << "\t������������ ���������������\n";
	std::system("pause");
	return 1;
}
int AddAdmin()
{
	std::system("cls");
	std::string key{};
	std::string str;
	int count{};
	time_t id;
	tm data;
	std::cout << "������� ����:   ";
	std::cin >> key;
	std::system("cls");
	if (key == "12345")
	{
		std::ifstream file;
		file.open("Admins.txt");
		Administrator* us = new Administrator;
		int n{};
		if (file.is_open())
		{
			if (file.peek() != EOF)
			{
				file >> n;
				for (int i = 0; i < n; i++)
				{
					file >> str;
					us->setName(str);
					file >> str;
					us->setSurname(str);
					file >> str;
					us->setLogin(str);
					file >> str;
					us->setPassword(str);
					file >> count;
					us->setAge(count);
					file >> id;
					us->setid(id);
					file >> us->m_level;
					file >> data.tm_year;
					file >> data.tm_mon;
					file >> data.tm_mday;
					file >> data.tm_hour;
					file >> data.tm_min;
					file >> data.tm_sec;
					us->setData(data);
					userad.push_back(*us);
				}
			}

			us = MakeAdmin();
			userad.push_back(*us);
			n++;
		}
		file.close();

		std::ofstream f;
		f.open("Admins.txt");
		if (f.is_open())
		{
			f << n << std::endl;
			for (int i = 0; i < n; i++)
			{
				f << userad[i].m_name << std::endl;
				f << userad[i].m_surname << std::endl;
				f << userad[i].getLogin() << std::endl;
				f << userad[i].getPassword() << std::endl;
				f << userad[i].getAge() << std::endl;
				f << userad[i].getid() << std::endl;
				f << userad[i].m_level << std::endl;
				f << userad[i].getData().tm_year << std::endl;
				f << userad[i].getData().tm_mon << std::endl;
				f << userad[i].getData().tm_mday << std::endl;
				f << userad[i].getData().tm_hour << std::endl;
				f << userad[i].getData().tm_min << std::endl;
				f << userad[i].getData().tm_sec << std::endl;
			}
		}
		f.close();
		for (int i = 0; i < n; i++)
		{
			userad.pop_back();
		}
		std::cout << "\t�� ����������������\n";
		return 2;
	}
	else {
		std::cout << "\t������ �������� ����\n";
		return 0;
	}
}
int Exi()
{
	std::system("cls");
	bool k = 0;
	std::cout << "\n\t���� �� ������ ����� �� ��������� - ������� 1, ���� �� ������ ��������� � ���������� ������ - ������� 0\n->  ";
	std::cin >> k;
	if (k == 0)
	{
		return 0;
	}
	else {
		exit(0);
	}
}
int InfUser()
{
	PrintUser(s_user);
	std::system("pause");
	return 1;
}
int InfAdmin()
{
	PrintAdmin(s_ad);
	std::system("pause");
	return 1;
}
int Add()
{
	std::system("cls");
	IEG::Item items[3]{ IEG::Item{"������������������ ��� ������������", AddUser}, IEG::Item{"������������������ ��� �������������", AddAdmin}, IEG::Item{"���������", Exi} };
	IEG::Cmenu menu{ "\t...�����������...", items, 3 };
	while (menu.runCommand()) {};
	return 2;
}
int Entry()
{
	std::system("cls");
	IEG::Item items[3]{ IEG::Item{"����� ��� ������������", EntUser}, IEG::Item{"����� ��� �������������", EntAdmin}, IEG::Item{"���������", Exi} };
	IEG::Cmenu menu{ "\t...����...", items, 3 };
	while (menu.runCommand()) {};
	std::system("cls");
	return 1;
}
int EntUser()
{
	std::system("cls");
	std::string s1{}, s2{};
	std::cout << "������� �����:   ";
	std::cin >> s1;
	std::cout << "������� ������:   ";
	std::cin >> s2;
	std::system("cls");
	ReadUser();
	bool temp = 0;
	for (int i = 0; i < user.size(); i++)
	{
		if ((user[i].getLogin() == s1) && (user[i].getPassword() == s2))
		{
			s_user = user[i];
			temp = 1;
			IEG::Item items[5]{ IEG::Item{"� ������������", InfUser},
				IEG::Item{"����� ������", Find},
				IEG::Item{"��� ������", PrintProd},
				IEG::Item{"�������", Basket},
				IEG::Item{"���������", Exi}, };

			IEG::Cmenu menu{ '\t' + s_user.m_name + ' ' + s_user.m_surname, items,   5};
			while (menu.runCommand()) {};

		}
	}
	if (temp == 0)
	{
		std::cout << "\n������� ������� ������\n";
		user.clear();
		std::system("pause");
		std::system("cls");
		return 1;
	}
	std::system("cls");
	user.clear();
	return 1;
}
int EntAdmin()
{
	std::system("cls");
	std::string s1{}, s2{};
	std::cout << "������� �����:   ";
	std::cin >> s1;
	std::cout << "������� ������:   ";
	std::cin >> s2;
	std::ifstream file;
	file.open("Admins.txt");
	Administrator* us = new Administrator;
	int n{};
	std::string str;
	int count{};
	time_t id;
	tm data;
	if (file.is_open())
	{
		file >> n;
		for (int i = 0; i < n; i++)
		{
			file >> str;
			us->setName(str);
			file >> str;
			us->setSurname(str);
			file >> str;
			us->setLogin(str);
			file >> str;
			us->setPassword(str);
			file >> count;
			us->setAge(count);
			file >> id;
			us->setid(id);
			file >> us->m_level;
			file >> data.tm_year;
			file >> data.tm_mon;
			file >> data.tm_mday;
			file >> data.tm_hour;
			file >> data.tm_min;
			file >> data.tm_sec;
			us->setData(data);
			userad.push_back(*us);
			
		}
	}
	file.close();
	bool temp = 0;
	std::system("cls");
	for (int i = 0; i < n; i++)
	{
		if ((userad[i].getLogin() == s1) && (userad[i].getPassword() == s2))
		{

			s_ad = userad[i];
			temp = 1;
			IEG::Item items[7]{ IEG::Item{"� ������������", InfAdmin},
				IEG::Item{"�������� �����", AddProd},
				IEG::Item{"������� �����", DeleteProduct},
				IEG::Item{"�������� ������ � ������", ChangeProduct},
				IEG::Item{"����� ������", Find},
				IEG::Item{"��� ������", PrintProd},
				IEG::Item{"���������", Exi} };

			IEG::Cmenu menu{ '\t' + s_ad.m_name + ' ' + s_ad.m_surname, items, 7 };
			while (menu.runCommand()) {};

		}
	}
	if (temp == 0)
	{
		std::cout << "\n������� ������� ������";
		userad.clear();
		return 2;
	}
	userad.clear();
	return 2;
}

int AddProd()
{
	std::system("cls");
	product.clear();
	ReadProduct();
	Product* prod = new Product;
	prod = MakeProduct();
	product.push_back(*prod);
	WriteProduct();
	product.clear();
	std::cout << "\n\t�����  ��������\n";
	delete prod;
	std::system("pause");
	return 2;
}
Product* MakeProduct()
{
	Product* prod = new Product;
	std::string tem{};
	unsigned int count;
	std::cin.get();
	std::cout << "\n������� �������� ������:  ";
	getline(std::cin, tem);
	prod->setName(tem);
	std::cin.ignore();
	std::cout << "\n������� ��� ������:   ";
	getline(std::cin, tem);
	prod->setType(tem);
	std::cout << "\n������� ���� ������:   ";
	std::cin >> count;
	prod->setPrice(count);
	std::cout << "\n������� ���������� �������:   ";
	std::cin >> count;
	prod->setCount(count);
	return prod;
	delete prod;
}


int DeleteProduct()
{
	ReadProduct();
	std::system("cls");
	time_t id{};
	Product* l = new Product;
	bool k = 0;
	std::cout << "\n������� id  ���������� ������ \n";
	std::cin >> id;
	std::vector<Product>::iterator it = product.begin();
	for (int i = 0; i < product.size(); i++)
	{
		k++;
		if (product[i].getid() == id)
		{
			*l = product[i];
			break;
		}
	}
	if (k == 0)
	{
		std::cout << "\n\t����� �� ������";
	}
	else {
		std::cout << "\n\t����� ������";
		product.erase(it+k-1);
	}
	delete l;
	WriteProduct();
	product.clear();
	std::system("pause");
	return 9;
}


int ChangeProduct()
{
	std::system("cls");
	ReadProduct();
	
	bool k = 0;
	time_t str{};
	std::cout << "\n�������  id:   ";
	std::cin >> str;
	for (int i = 0; i < product.size(); i++)
	{
		if (product[i].getid() == str)
		{
			filt = product[i];
			k = 1;
		}
	}
	if (k == 1)
	{
		IEG::Item items[5]{ IEG::Item{"�������� ��������", ChangeName},
			IEG::Item{"�������� ���", ChangeType},
			IEG::Item{"�������� ����", ChangePrice},
			IEG::Item{"�������� ����������", ChangeCount},
			IEG::Item{"���������", Exi}
		};
		IEG::Cmenu menu{ "\t" + s_ad.m_name + ' ' + s_ad.m_surname, items, 5 };
		while (menu.runCommand()) {};
	}
	else {
		std::cout << "\n����� �� ������.\n";
		std::system("pause");
	}
	product.clear();
	return 2;
}
int ChangeName()
{
	ReadProduct();
	std::system("cls");
	std::string str{};
	std::cin.get();
	std::cout << "\n������� ����� ��������:   ";
	getline(std::cin, str);
	filt.setName(str);
	for (int i = 0; i < product.size(); i++)
	{
		if (product[i].getid() == filt.getid())
		{
			product[i].setName(str);
		}
	}
	std::cout << "\n\t�������� ��������\n";
	WriteProduct();
	product.clear();
	std::system("pause");
	return 1;
}
int ChangeType()
{
	ReadProduct();
	std::system("cls");
	std::string str{};
	std::cin.get();
	std::cout << "\n������� ����� ���:   ";
	getline(std::cin, str);
	filt.setType(str);
	for (int i = 0; i < product.size(); i++)
	{
		if (product[i].getid() == filt.getid())
		{
			product[i].setType(str);
		}
	}
	std::cout << "\n\t��� �������\n";
	WriteProduct();
	product.clear();
	std::system("pause");
	return 2;
}

int ChangePrice()
{
	ReadProduct();
	std::system("cls");
	unsigned int str{};
	std::cout << "\n������� ����� ����:   ";
	std::cin >> str;
	filt.setPrice(str);
	for (int i = 0; i < product.size(); i++)
	{
		if (product[i].getid() == filt.getid())
		{
			product[i].setPrice(str);
		}
	}
	std::cout << "\n\t���� ��������\n";
	WriteProduct();
	product.clear();
	std::system("pause");
	return 3;
}
int ChangeCount()
{
	ReadProduct();
	std::system("cls");
	unsigned int str{};
	std::cout << "\n������� ����� ����� �������:   ";
	std::cin >> str;
	filt.setCount(str);
	for (int i = 0; i < product.size(); i++)
	{
		if (product[i].getid() == filt.getid())
		{
			product[i].setCount(str);
		}
	}
	std::cout << "\n\t���������� ������� ��������\n";
	WriteProduct();
	product.clear();
	std::system("pause");
	return 1;
}

int Basket()
{
	ReadProduct();

	if (s_user.getBasket().size() == 0)
	{
		std::cout << "\n\t������� ���.";
	}
	else
	{
		std::cout << "\n\t������ �������:";

		for (int i = 0; i < s_user.getBasket().size(); i++)
		{
			std::cout << "\n\t� " << i + 1;
			for (int j = 0; j < product.size(); j++)
			{
				if (product[j].getid() == s_user.getBasket()[i])
				{
					PrintProduct(product[j]);
				}
			}
			std::cout << std::endl;
		}
	}
	product.clear();
	std::system("pause");
	return 3;
}

int Find()
{
	std::system("cls");
	ReadProduct();
	temp.clear();
	int k{};
	std::string str{};
	std::cin.get();
	std::cout << "\n������� ��� ������:   ";
	getline(std::cin, str);
	for (int i = 0; i < product.size(); i++)
	{
		if (product[i].getType() == str)
		{
			temp.push_back(product[i]);
		}
	}
	for (int i = 0; i < temp.size(); i++)
	{
		std::cout << "\n\t� " << i + 1;
		PrintProduct(temp[i]);
		product.clear();
		std::system("pause");
	}
	if (temp.size() == 0)
	{
		std::cout << "\n\t������ �� �������";
		std::system("pause");
		product.clear();
	}
	std::cout << "\n1. �������� ����� � �������\n0. ���������\n";
	std::cin >> k;
	if (k == 0)
	{
		temp.clear();
		return 5;
	}
	else
	{
		std::cout << "\n������� ����� ������:   ";
		std::cin >> k;
		std::vector<time_t> t;
		t.push_back(temp[k - 1].getid());
		std::cout << "\n����� ��������\n";
		s_user.setBaskid(t[0]);
		std::system("pause");
		for (int i = 0; i < user.size(); i++)
		{
			if (user[i].getid() == s_user.getid())
			{
				user[i] = s_user;
			}
		}
		WriteUser();
		temp.clear();
		return 5;
	}
}

void ReadProduct()
{
	product.clear();
	std::ifstream file;
	file.open("Product.txt");
	Product* prod = new Product;
	std::string tem;
	int n{};
	unsigned int count;
	time_t id;
	if (file.is_open())
	{
		file >> n;
		for (int i = 0; i < n; i++)
		{
			file.get();
			getline(file, tem);
			prod->setName(tem);
			getline(file, tem);
			prod->setType(tem);
			file >> count;
			prod->setPrice(count);
			file >> count;
			prod->setCount(count);
			file >> id;
			prod->setid(id);
			product.push_back(*prod);
		}
	}
	file.close();
}
void WriteProduct()
{
	std::ofstream file;
	file.open("Product.txt");
	Product* prod = new Product;
	if (file.is_open())
	{
		file << product.size() << std::endl;
		for (int i = 0; i < product.size(); i++)
		{
			file << product[i].getName() << std::endl;
			file << product[i].getType() << std::endl;
			file << product[i].getPrice() << std::endl;
			file << product[i].getCount() << std::endl;
			file << product[i].getid() << std::endl;
		}
	}
	file.close();
}
int PrintProd()
{
	ReadProduct();
	if (product.size() == 0)
	{
		std::cout << "\n\t������� ���.";
	}
	else
	{
		std::cout << "\n\t������ �������:";
	}
	for (int i = 0; i < product.size(); i++)
	{
		std::cout << "\n\t� " << i + 1;
		PrintProduct(product[i]);
		std::cout << std::endl;
	}
	product.clear();
	std::system("pause");
	return 3;
}
void PrintProduct(Product& prod)
{
	std::cout << "\n��������:   " << prod.getName(); 
	std::cout << "\n���:   " << prod.getType(); 
	std::cout << "\n����:   " << prod.getPrice(); 
	std::cout << "\n����������:   " << prod.getCount(); 
	std::cout << "\nID:   " << prod.getid(); 
}