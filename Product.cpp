#include "Product.h"
#include <ctime>

Product::Product() :m_name("___"), m_type("___"), m_price(0), m_count(0)
{ m_id = time(0); };
Product::Product(std::string name, std::string type, unsigned int price, unsigned int count) : m_name(name), m_type(type), m_price(price), m_count(count) {
	m_id = time(0);
};
void Product::setName(std::string name)
{
	m_name = name;
}
void Product::setType(std::string type)
{
	m_type = type;
}
void Product::setPrice(unsigned int price)
{
	m_price = price;
}
void Product::setCount(unsigned int count)
{
	m_count = count;
}
void Product::setid(time_t id)
{
	m_id = id;
}
std::string Product::getName()
{
	return m_name;
}
std::string Product::getType()
{
	return m_type;
}
unsigned int Product::getPrice()
{
	return m_price;
}
unsigned int Product::getCount()
{
	return m_count;
}
time_t Product::getid()
{
	return m_id;
}