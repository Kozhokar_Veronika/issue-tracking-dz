#include "CMenu.h"
#include <iostream>
#include <fstream>
using namespace IEG;
const int Numb = 50;

Cmenu::Cmenu(std::string title = nullptr, ItemMenu* items = nullptr, unsigned int count = 0) : m_title(title), m_items(items), m_count(count) {}

int Cmenu::getSelect()
{
	return this->m_select;
}
void Cmenu::setSelect(int select)
{
	m_select = select;
}
bool Cmenu::getRunning()
{
	return this->m_running;
}
std::string Cmenu::getTitle()
{
	return this->m_title;
}
unsigned int Cmenu::getCount()
{
	return this->m_count;
}
ItemMenu* Cmenu::getItems()
{
	return this->m_items;
}

void Cmenu::printItems () const
{
	std::cout << std::endl << m_title << std::endl;
	for (int i = 0; i < m_count; i++)
	{
		std::cout << i + 1 << ". ";
		m_items[i].printItem();
	}
}
namespace IEG
{
	std::ostream& operator<< (std::ostream& out, const Cmenu& menu)
	{
		system("cls");
		menu.printItems();
		return out;
	}


	std::istream& operator>> (std::istream& in, Cmenu& menu)
	{
		menu.m_select = menu.readEnter();
		return in;
	}
}
int Cmenu::runCommand()
{ 
	std::cout << *this;
	std::cin >> *this;
	return this->m_select;
}
int Cmenu::readEnter()
{
	setlocale(LC_ALL, "rus");
	std::cout << "������� ����� ������ ����: \n --> ";
	std::cin >> m_select;
	if ((m_select != 0) && (m_select <= m_count))
	{
		return m_items[m_select - 1].run();
	}
	else
	{
		std::cout << "������ ����� ����������� \n";
		m_select = 0;
		return 0;
	}
}