#include "Person.h"

Person::Person(std::string name, std::string surname, int age) : m_name(name), m_surname(surname), m_age(age) {};
Person::Person() : m_name(" "), m_surname(" "), m_age(0) {};

void Person::setName(std::string name)
{
	m_name = name;
}
void Person::setSurname(std::string surname)
{
	m_surname = surname;
}
void Person::setAge(int age)
{
	m_age = age;
}

std::string Person::getName()
{
	return m_name;
}
std::string Person::getSurname()
{
	return m_surname;
}
int Person::getAge()
{
	return m_age;
}